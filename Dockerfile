FROM python:3.8-slim

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1

WORKDIR /app

ENV SECRET_KEY="TO_CHANGE"
ENV DEBUG=0
ENV DJANGO_ALLOWED_HOSTS="localhost 0.0.0.1"
ENV SQL_ENGINE="django.db.backends.sqlite3"
ENV SQL_DATABASE="db.sqlite3"
ENV SQL_USER="user"
ENV SQL_PASSWORD="password"
ENV SQL_HOST="localhost"
ENV SQL_PORT=5432

COPY Pipfile .
COPY Pipfile.lock .
COPY vapormap ./vapormap

RUN apt-get update && \
    apt-get install --no-install-recommends -y python3-dev default-libmysqlclient-dev build-essential && \
    pip install --no-cache-dir pipenv==2021.11.23 && \
    pipenv install --system --deploy --ignore-pipfile && \
    python vapormap/manage.py collectstatic --no-input && \
    pipenv --clear && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app/vapormap

CMD ["gunicorn", "vapormap.wsgi", "--bind", "0.0.0.0:8000"]
